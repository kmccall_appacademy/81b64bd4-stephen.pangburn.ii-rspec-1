# simon_says.rb

def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, reps = 2)
  reps.times.inject([]) { |acc, _rep| acc << string }.join(" ")
end

def start_of_word(string, numletters)
  string[0...numletters]
end

def first_word(string)
  string.split(" ")[0]
end

def titleize(string)
  little_words = ["and", "the", "over", "to", "a", "an"]

  title = string.split.map do |word|
    word[0] = word[0].upcase if !little_words.include?(word)
    word
  end
  title[0][0] = title[0][0].upcase

  title.join(" ")
end
