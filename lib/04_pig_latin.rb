# pig_latin.rb

def translate(sentence)
  translated = ""
  sentence.split.each do |word|
    translated << pigify(word) << " "
  end
  translated[0..-2]
end

def pigify(word)
  vowels = "aeiou"
  beginning = ""
  cutoff = 0

  word.chars.each_with_index do |letter, i|
    if vowels.include?(letter) && word[i - 1..i] != 'qu'
      cutoff = i
      break
    else
      beginning << letter
    end
  end
  word[cutoff..-1] + beginning + "ay"
end
