# calculator.rb

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  return 0 if arr.empty?
  arr.inject(:+)
end

def multiply(elem, elem2 = nil)
  if elem2 == nil
    return 0 if elem.empty?
    elem.inject(:*)
  else
    elem * elem2
  end
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  (1..num).inject(:*) || 1 # Refactored

  # return 1 if num == 0
  # result = 1
  #
  # num.downto(1) do |n|
  #   result *= n
  # end
  # result
end
